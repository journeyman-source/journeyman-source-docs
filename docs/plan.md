## Mission

Our missing is to show that programming and contributing to Open Source can be fun, valuable, and easy.

## Why "Journeyman Source"?

This title is inspired from the Open Source culture and the spirit of the Journeyman.
The idea is that we are unboxing, promoting, and hacking away at this Journeyman spirit.

It's also inspired from the very cool video game [The Journeyman Project](https://youtu.be/B9Gz76nKYeE).

## Audience

- People interested in programming
- Programmers interested in contributing to open source
- Programmers looking for community

## Format

There will be two types of content created: a long pair programming livestream with a guest, and a short 3-5 minute highlights:

1. Brief intro
1. Brief interview
1. Intro to OS project and issue
1. Pair programming
1. Lessons learned

## Frequency

Let's shoot for 1 episode a week for now :)

