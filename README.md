# Journeyman Source - Docs

This project contains some documents which help define the activities of the
Journeyman Source.

- [The Plan](docs/plan.md)
- [The Backlog](docs/backlog.md)
